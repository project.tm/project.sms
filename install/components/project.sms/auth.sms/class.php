<?php

use Bitrix\Main\Application,
    Bitrix\Main\Loader;

class ProjectSmsAuthSms extends CBitrixComponent {

    public function executeComponent() {
//        preDebugStart();
        $this->arResult['ERROR'] = '';
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arResult['STEP'] = 'phone';
        if (Loader::includeModule('project.sms')) {
//            $this->arResult['ERROR'] = 'сервис временно не доступен';
//            if(0)
            if ($request->get('isPhone')) {
                $this->actionPhone($request);
            } elseif ($request->get('isSms') and self::getPhone()) {
                $this->actionSms($request);
            } else {
                $this->actionDefault();
            }
        } elseif ($_POST) {
            $this->arResult['ERROR'] = 'Ошибка сервера, повторите запрос позднее';
        }

        $this->arResult["SECURE_AUTH"] = false;
        if (!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y') {
            $sec = new CRsaSecurity();
            if (($arKeys = $sec->LoadKeys())) {
                $sec->SetKeys($arKeys);
                $sec->AddToForm('system_auth_form' . $arResult["RND"], array('USER_PASSWORD'));
                $this->arResult["SECURE_AUTH"] = true;
            }
        }

//        pre($this->arResult['CAPTCHA']);
        if ($this->arResult['CAPTCHA']) {
            global $APPLICATION;
            $this->arResult["CAPTCHA_CODE"] = $APPLICATION->CaptchaGetCode();
        }

        $this->includeComponentTemplate();
    }

    static public function getPhone() {
        return $_SESSION[__CLASS__];
    }

    static public function setPhone($phone) {
        $_SESSION[__CLASS__] = $phone;
    }

    static public function clearPhone() {
        unset($_SESSION[__CLASS__]);
    }

    public function initCaptcha($captcha) {
        $this->arResult['CAPTCHA'] = (bool)$captcha;
//        pre($this->arResult['CAPTCHA']);
    }

    private function actionPhone($request) {
        $this->arResult['PHONE'] = trim($request->get('PERSONAL_PHONE'));
        if (empty($this->arResult['PHONE'])) {
            $this->arResult['ERROR'] = 'Введите телефон';
        } else {
            $phone = str_replace(array(' ', '(', ')', '-'), '', $this->arResult['PHONE']);
            $is = preg_match('~(\+7[0-9]{10})~', $phone);
            if ($is and strlen($phone) == 12) {
//                        pre($phone, $is, strlen($phone), $this->arResult['PHONE']);
                if ($arUser = Project\Sms\User::initCode($phone)) {
                    if (isset($arUser['SMS']['ERROR'])) {
                        $this->arResult['ERROR'] = $arUser['SMS']['ERROR'];
                    } else {
                        self::setPhone($phone);
//                        pre($arUser['SMS']);
                        $is = Project\Sms\Send::sms($phone, 'Авторизация на igromafia.ru: ' . $arUser['SMS']['CODE']);
                        if ($is) {
                            $this->arResult['STEP'] = 'sms';
                            $this->initCaptcha($arUser['UF_CAPTCHA']);
                        } else {
                            $this->arResult['ERROR'] = 'Ошибка сервера, повторите запрос позднее';
                        }
                    }
                } else {
                    $this->arResult['ERROR'] = 'Пользователь не найден';
                }
            } else {
                $this->arResult['ERROR'] = 'Введите корректный телефон';
            }
        }
    }

    private function actionSms($request) {
        if ($arUser = Project\Sms\User::getCode(self::getPhone())) {
            $this->initCaptcha($arUser['UF_CAPTCHA']);
//            pre($arUser['SMS']);
            if (empty($arUser['SMS']['ACTIVE'])) {
                $this->arResult['ERROR'] = 'Время сесии истекло';
            } else {
                $this->arResult['STEP'] = 'sms';
                global $APPLICATION;
                if ($this->arResult['CAPTCHA'] and ! $APPLICATION->CaptchaCheckCode($request->get('captcha_word'), $request->get('captcha_sid'))) {
                    $this->arResult['ERROR'] = 'Введите правильный код с картинки';
                }

                if (empty($this->arResult['ERROR'])) {
                    if ($arUser['SMS']['CODE'] === $request->get('CODE')) {
                        $this->arResult['STEP'] = 'auth';
                        self::clearPhone();
                        Project\Sms\User::succes($arUser);
                        global $USER;
                        $USER->Authorize($arUser['ID']);
                    } else {
                        $arUser['SMS']['LIMIT'] --;
                        $this->initCaptcha(Project\Sms\User::fail($arUser));
                        if ($arUser['SMS']['LIMIT'] < 1) {
                            $this->arResult['STEP'] = 'phone';
                            $this->arResult['ERROR'] = 'Кончились попытки, попробуйте снова';
                        } else {
                            switch ($arUser['SMS']['LIMIT']) {
                                case 1:
                                    $message = 'осталась одна попытка';
                                    break;

                                default:
                                    $message = 'осталось ' . $arUser['SMS']['LIMIT'] . ' попытки';
                                    break;
                            }
                            $this->arResult['ERROR'] = 'Код не верный, ' . $message;
                        }
                    }
                }
            }
        } else {
            $this->arResult['ERROR'] = 'Время сесcии истекло';
        }
    }

    private function actionDefault() {
        $phone = self::getPhone();
        if ($phone) {
            if ($arUser = Project\Sms\User::getCode($phone)) {
//                pre($arUser['SMS']);
                if (!empty($arUser['SMS']['ACTIVE'])) {
                    $this->arResult['STEP'] = 'sms';
                    $this->initCaptcha($arUser['UF_CAPTCHA']);
                }
            }
        }
    }

}
