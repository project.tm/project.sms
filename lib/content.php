<?php

namespace Project\Sms;

class Content {

    static public function curl($url, $isJson = true) {
        $arResult = array();
        try {
//            $arResult = Cache::get($url);
            if(empty($arResult)) {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_USERAGENT, 'sms/1.0');
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                $arResult = curl_exec($curl);
                curl_close($curl);
//                Cache::set($url, $arResult);
            }
//            pre($url, $arResult);
            if ($isJson) {
                $arResult = json_decode($arResult, true);
            }
        } catch (Exception $exc) {
            
        }
        return $arResult;
    }

}
