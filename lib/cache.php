<?php

namespace Project\Sms;

class Cache {

    static public function getPsth($url) {
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/sms/' . sha1($url);
        CheckDirPath($path);
        pre($path);
        return $path;
    }

    static public function set($url, $arResult) {
        file_put_contents($url, $arResult);
    }

    static public function get($url) {
        if (file_exists($url = self::getPsth($url))) {
            return file_get_contents($url);
        }
        return false;
    }

}
