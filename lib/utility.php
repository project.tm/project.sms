<?php

namespace Project\Sms;

class Utility {

    static public function filterPhone($phone) {
        return preg_replace('~([^0-9])~S', '', $phone);
    }

}
