<?

namespace Project\Sms\Event;

use CUser,
    Project\Sms\Utility,
    Project\Discount\Config;

class User {

    public static function OnBeforeUserUpdate(&$arFields) {
        if(isset($arFields['PERSONAL_PHONE'])) {
            $arFields['UF_PHONE'] = Utility::filterPhone($arFields['PERSONAL_PHONE']);
        }
    }

}
